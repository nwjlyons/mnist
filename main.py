from bottle import route, run, template, request
import numpy as np

from classifier import classifier


clf = classifier()


@route('/', method=['GET', 'POST'])
def index():
    if request.is_ajax:
        img = np.array([int(pixel) for pixel in request.POST['image'].split(",")], dtype=np.uint8)
        return {"guess": int(clf.predict([img])[0])}
    return template('index.html')


run(host='localhost', port=8085, debug=True)
