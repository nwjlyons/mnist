import os

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split


def classifier():
    if os.path.isfile('RandomForestClassifier.pkl'):
        print("Loading RandomForestClassifier")
        clf = joblib.load('RandomForestClassifier.pkl')
    else:
        print("Training RandomForestClassifier")
        train = pd.read_csv("train.csv")
        features = train.columns[1:]
        X = train[features]
        y = train['label']
        X_train, X_test, y_train, y_test = train_test_split(X/255.,y,test_size=0.1,random_state=0)

        clf = RandomForestClassifier()
        clf.fit(X_train, y_train)
        joblib.dump(clf, 'RandomForestClassifier.pkl')

    return clf
