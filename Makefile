deploy:
	ssh neillyons.io "supervisorctl stop mnist"
	rsync --exclude-from=".rsyncignore" -r -P . neillyons.io:/srv/www/mnist.neillyons.io/
	ssh neillyons.io "supervisorctl start mnist"
